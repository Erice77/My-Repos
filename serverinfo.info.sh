#!

#play the date
echo "Date: $(date)"

# Display the last 10 users who logged into the server
echo "Last 10 logged-in users:"
last -n 10

# Display swap space information
echo "Swap Space:"
free -h | grep Swap

# Display the kernel version
echo "Kernel Version:"
uname -r

# Display the IP address
echo "IP Address:"
ip addr show | awk '/inet / {print $2}'


